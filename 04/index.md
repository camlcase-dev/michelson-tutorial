# Part IV: Michelson Data Structures

Now that we've got a little Michelson experience under our belt, in this chapter
we're going to go over some different Michelson types and the instructions that
operate on them.

For the purposes of this chapter, it's going to be useful for us to have a
little test-bed for testing types, data constructors and instructions:

```
# const.tz
parameter unit;
storage unit;
code { DROP;
     # begin arbitrary
       PUSH string "foobar";
       DROP;
     # end arbitrary, stack must be empty
       PUSH unit Unit; NIL operation; PAIR;
     };
```

This contract does nothing, by design. However it has one useful property for
testing: We can write code in between the commented parenthesis that performs
arbitrary stack manipulations, so long as the stack is empty by the end of the
block. This will let us test out manipulating some different data
types in a type-checked way.

## Pairs

We've seen the `PAIR` instruction before, but let's look at the definition
again:

```
code  PAIR
stack a : b : S  =>  (Pair a b) : S
type  'a : 'b : 'A -> pair 'a 'b : 'A
```

You may be asking, what is the difference between `PAIR`, `Pair` and `pair`?

- `PAIR` is an instruction, whose stack manipulation operation is defined above
- `pair 'l 'r` is a type constructor that takes two types, a left-hand type and
  a right-hand type and composes them together into a product type.
- `Pair` is a data constructor that takes two data values and composes them
  together into a pair. If value `x` has type `a` and value `y` has type `b`
  then `Pair x y` has type `pair a b`.

Try putting the following operation into our `const.tz` contract (making sure to
add appropriate `DROP`s so it typechecks):

```
PUSH (pair nat int) (Pair 1 1);
```

If you recall, `PUSH` takes a type parameter and a value, then pushes a value
with that type onto the stack. If you typecheck the contract now with `-v`, you
should see the type `pair nat int` in the output.

To further satisfy your curiosity, try replacing the left-hand side with a
negative number to see what happens.

Pairs can be constructed with the `PAIR` operation and with the `PAPAIR` family
of macros.

---

**A brief aside on macros**:

Michelson has several macros that are expanded by the parser into sequences of
operations. You can read about the details here: [https://tezos.gitlab.io/whitedoc/michelson.html#macros](https://tezos.gitlab.io/whitedoc/michelson.html#macros)

In practice, you usually don't have to pay attention to the details of the
macro expansions, and can mostly just use them as normal. The one caveat to this
is that macros are expanded before contract origination, so only sequences of
regular (also called "primitive") operations appear on the Tezos chain. This can
effect the transparency and legibility of the contract. Furthermore, since
short macros can expand to relatively longer sequences of operations, use of
macros can sometimes cause the author of a contract to underestimate gas
consumption.

---

### Constructing pairs with the `PAPAIR` macro

The `PAPAIR` family of macros allows for the construction of arbitrary pair
structures from values on the stack.

For example, `PAPAIR` will turn `x : y : z : []` into `(Pair x (Pair y z)) :
[]`. Try the following sequence:

```
PUSH int 1;
PUSH int 2;
PUSH int 3;
PAPAIR;
```

If instead we wanted `(Pair (Pair x y) z)` with the nested pair on the left, we
would use: `PPAIIR`.

This macro is a little confusing and will probably be changed in a future
version of Michelson, but the essential logic is that every `P` stands for
`Pair`, every `A` for a left-hand leaf, every `I` for a right-hand leaf and `R`
to terminate the macro (strictly speaking `R` is unnecessary).

The tree structure for `PPAIIR` look like:

```
     P
    / \
   P   I
  / \
 A   I
```

While that for `PAPAIR` looks like:

```
     P
    / \
   A   P
      / \
     A   I
```

Another way to think about this is by taking the macro string and applying
some transformations to it:

Let's start with:

```
PAPPAIIR
```

First add spaces.

```
P A P P A I I R
```

Replace each `P` with an open-paren and a `pair`:

```
(pair A (pair (pair A I I R
```

Add close-parens after every `I`:

```
(pair A (pair (pair A I) I) R
```

And finally replace `R` with as many close-parens as needed to complete every
remaining dangling open paren

```
(pair A (pair (pair A I) I) )
```

The elements on the stack will now be assigned to this tree in the order they
appear in the above string.

I encourage experimenting with these macros on your own until you get a feel for
them.

### Destructing Pairs with `UNPAIR`

The inverse macro to `PAPAIR` is the `UNPAIR` family of macros. These take
a nested pair tree at the top of the stack and place its leaves on the stack in
according to the order of the leaf in the macro string (as above).

Try running the following in our `const.tz`, adding `DROP`s afterwards to
clean-up for the type-checker.

```
PUSH int 1; PUSH int 2; PUSH int 3; PUSH int 4;
PAPPAIIR;
UNPAPPAIIR;
```

The `UNPAPPAIIR` simply reverses the `PAPPAIIR`.

However, since pair trees are nested pairs, we can actually use the `UNPAIR`
macro to partially destruct the tree:

```
PUSH int 1; PUSH int 2; PUSH int 3; PUSH int 4;
/* [ int : int : int : int ] */ ;
PAPPAIIR;
/* [ pair int (pair (pair int int) int) ] */ ;
UNPAPAIR;
/* [ int : pair int int : int ] */ ;
```

This might seem a little confusing at first. Essentially what's going on is that
that a `PAPPAIIR` tree is actually just a `PAPAIR` tree with one of the leaves
replaced by a pair like so: `PAP(PAI)IR`.

### Selecting leaves with `CAR` and `CDR` macros

We can also destruct pair trees down to a single leaf with the `CAR` and `CDR`
primitive operations, and the `CADR` family of macros.

As covered in chapter 1, `CAR` selects the left-hand side:
```
PUSH int 1; PUSH nat 1; PUSH string "1"; PUSH bool True;
/* [ bool : string : nat : int ] */ ;
PAPPAIIR;
/* [ pair bool (pair (pair string nat) int) ] */ ;
CAR;
/* [ bool ] */ ;
```

`CDR` selects the left-hand side:
```
PUSH int 1; PUSH nat 1; PUSH string "1"; PUSH bool True;
/* [ bool : string : nat : int ] */ ;
PAPPAIIR;
/* [ pair bool (pair (pair string nat) int) ] */ ;
CDR;
/* [ (pair (pair string nat) int) ] */ ;
```

We can combine sequences of `CAR` and `CDR` using their macro (which I call the
`CADR` macro). `CDDR` is the same as `CDR; CDR;`:

```
PUSH int 1; PUSH nat 1; PUSH string "1"; PUSH bool True;
/* [ bool : string : nat : int ] */ ;
PAPPAIIR;
/* [ pair bool (pair (pair string nat) int) ] */ ;
CDDR;
/* [ int ] */ ;
```

And `CDADR` is `CDR; CAR; CDR:`

```
PUSH int 1; PUSH nat 1; PUSH string "1"; PUSH bool True;
/* [ bool : string : nat : int ] */ ;
PAPPAIIR;
/* [ pair bool (pair (pair string nat) int) ] */ ;
CDADR;
/* [ nat ] */ ;
```

Essentially, instead of writing a lot of `C`'s, `R`'s and semicolons, the `CADR`
macro lets us be a little more concise by focusing on the parts that matter: the
`A`'s and `D`'s.

As an aside, the names ["CAR" and "CDR" come from
Lisp](https://en.wikipedia.org/wiki/CAR_and_CDR) where they are the functions
that select the left-hand and right-hand sides, respectively, of a cons cell.
Personally I find their use in Michelson very confusing, since as you will see
in the next chapter, Michelson's `CONS` operation is a `list` constructor and
has nothing whatsoever to do with `CAR` and `CDR`.

## The option type

We covered the option type briefly in the previous chapter, but here we will
include two additional `option` operations that are often useful:

```
# pack a present optional value
code SOME;
stack    x : S  => Some x : S
type  :: a : A -> (option a) : A

# the absent optional value
code NONE a;
stack    S => None : S
type  :: A -> (option a) : 'A
```

Notice that `NONE` takes a type parameter like `PUSH`. This is because the data
constructor `None` is the same for any type `option a`. So despite the fact that
e.g.:

```
None :: option bool
None :: option int
```

have the same data value, their types are different. This means that, for
example, an `IF_NONE` or `IF_SOME` that switches on an `option bool` will not
accept a `None :: option int`.

## The union type `or`

An `or` type is actually very much like an `option type`. Whereas an `option`
time represents the either a typed value or a typed null value, the `or` type
represents the possibility of two distinct typed values, that may be of
different types.  For example, an `or int bool` type can be either an `Left 1`,
or a `Right True`.

The `or` type has only a few operations, like `option`. We can construct unions
from simple data values:

```
# Pack a value in a union (left case).
code LEFT 'b;
stack    v : S  =>  (Left v) : S
type :: 'a : 'S ->   or 'a 'b : 'S

#Pack a value in a union (right case).
code RIGHT 'a:
stack    v : S  =>  (Right v) : S
type :: 'b : 'S   ->   or 'a 'b : 'S
```

And we can switch on whether the union is its left or right-hand side:

```
# Inspect a value of a variant type.
code  IF_LEFT bt bf
stack   (Left a) : S  =>  bt / a : S
        (Right b) : S  =>  bf / b : S
type :: or 'a 'b : 'S   ->   'c : 'S
   iff   bt :: [ 'a : 'S -> 'c : 'S]
         bf :: [ 'b : 'S -> 'c : 'S]

# Inspect a value of a variant type.
code  IF_Right bt bf
stack   (Left a) : S  =>  bf / a : S
        (Right b) : S  =>  bt / b : S
type :: or 'a 'b : 'S   ->   'c : 'S
   iff   bf :: [ 'a : 'S -> 'c : 'S]
         bt :: [ 'b : 'S -> 'c : 'S]
```


This kind of type is often called a "union" or "sum" type, in contrast to
something like `pair`, which could be called an "product" or "intersection"
type. Combining product types and sum types in a single structure enables us to
us the ubiquitous (and powerful!) functional programming concept: the [Algebraic
Data Type (or ADT)](https://en.wikipedia.org/wiki/Algebraic_data_type)

An example in Michelson of an `ADT` would be:

```
(or
  (or (pair string string)
      (pair string int)
  )
  bytes
)
```

Which could represent, for example, a (simplified) possible result type from
calling an oracle contract:

```
(or
  (or (pair string string) # external error with error message
      (pair string int)    # internal error with error code
  )
  bytes                    # contract returns oracular bytes
)
```

If you set up an oracle contract to return this type, then any caller could
switch on the union to gain some type safety on whether or not your oracle
encountered any errors.

## Exercises

1. Which `CADR` family macro would you use to access the `nat` value in
   ```
   (pair
     (pair
       (pair
         (pair int (pair bool int))
         (pair bool (pair nat))
       )
       string
     )
     (pair bool int)
   )
   ```

2. Construct a concrete value with the type from question 1 by pushing values
   onto the stack and calling a `PAPAIR` family macro.

3. Using a single `UNPAIR` family macro, partially destruct the value from
   question 2 so that a value of type
   ```
   (pair
     (pair int (pair bool int))
     (pair bool (pair nat))
   )
   ```
   is at the top of the stack.

4. Write a contract called `switch.tz` that takes an
   `(or int string)` parameter and a `(pair int string)` storage.
   When called with a string, the contract should replace the string in storage
   with the new string. When called with an `int` it should add the parameter to
   the `int` in its storage.

5. Now write `switch2.tz` that takes an `option (or int string)` parameter
   and an `(pair (option int) (option string))` storage. The operation of the
   contract is the same as exercise 4, except that if the parameter is `None`
   then the `option int` and `option string` in the storage will be set to
   `None`. If the parameter is `Some (Pair n str)` and the storage is
   `(Pair None None)` then set the storage to `Pair (Some n) (Some str)`.
